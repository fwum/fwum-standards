#I. Code, non-code, and definitions

1. Code is defined as any text that is relevant to compilation. Any comments or discarded whitespace is not considered relevant to compilation.

2. Whitespace includes the space character (' '), the tab character ('\t'), the newline character ('\n'), and the carriage return character ('\r').

	a. A line break either the newline or carriage return character. They are treated the same by Fwum.

3. Comments are text that is ignored by the compiler.

	a. A single line comment begins with `//`. This comment continues until a newline character or the end of the source file is reached.

	b. Multi-line comments begin with `/*` and end with `*/`. They can be nested, meaning they are only closed when there are as many closing `*/` as opening `/*`

4. Identifiers may start with an underscore or an alphabetical character. They may be proceeded by underscores, letters, and/or numbers.

	a. Fwum is case sensitive. The identifier `a` is not the same as the identifier `A`.

	b. Only specific built-in identifiers may start and end with two underscores.

	c. No identifiers may start with three underscores.

5. Character literals start and end with a single quote, and evaluates to exactly one character.

	a. Most characters are represented by a single character literal.

	b. Some require special codes to produce, like the newline character `'\n'` or the carriage return character `'\r'`

6. String literals start and end with double quotes.

	a. They may not contains any line breaks.

7. Number literals may only contains numbers and a single period `.`

#II. Top-level elements

1. Top-level elements are defined by not being nested in braces. Any element that has as many preceding ending braces (`}`) as opening braces (`{`) is a top-level element.

2. The top level element `export` is used to determine what functions and globals the module will expose to other code that imports it.

	a. Exports take the form of `export<whitespace optional>{/*Semicolon-seperated names*/}`, where `<whitespace>` is replaced by any whitespace token (defined in I.2). `Semicolon-seperated names` is replaced by a list of identifiers defined in the function, separated by semicolons.

	b. No trailing semicolon is allowed on the last element.

	c. There may be as many or few whitespace tokens between `export` and the `{`.

	d. If any element in the `export` block is not an identifier contained in the source file, the compiler should produce a meaningful error indicating this.

3. The top level element `import` is used to include other source modules. They may be in the same folder or elsewhere on the compiler search path. `import` takes the form of `import<whitespace mandatory><namespace>;`

	a. Namespace must form a valid identifier literal.

	b. Imports must end in a semicolon.

4. The top level element `using` is used to bring elements from a namespace into the global context. It takes the form of `using<whitespace mandatory><namespace>;`

	a. The namespace must have a semicolon immediately after it.

5. The top level element `cimport` is used to import C modules to the FFI. It takes the form of `cimport<whitespace mandatory><cname>;`

	a. `<cname>` may be any string that does not contain whitespace or a semicolon.

	b. There may not be any whitespace between `<cname>` and the semicolon.

6. The top level element `struct` is used to declare a data type. It takes the form of `struct<whitespace mandatory><structname><whitespace optional>{<struct members>}`

	a. `<structname>` is the name given to the struct, which must be a valid, unique identifier.

	b. `<struct members` are all of the members with their types, each in the form `<member><whitespace optional>:<whitespace optional><type>;` where `<member>` is a valid identifier and `<type>` is a type that has been defined or imported.

7. The top level element `template` is used to declare a template type. It takes the form of `template<whitespace mandatory><typename>;`

	a. `<typename>` is the identifier that the template type will be referred to by.

8. The top level element `require` is used to force a template type to have certain functions. It takes the form of `require<whitespace mandatory><template name><whitespace optional>{<function signatures>}`

	a. `<template name>` is the name of a previously defined template type.

	b. `<function signatures>` is a list of function signatures, which are composed of the return type, the function name, and the types of the parameters. For example, `myReturn myFunc(param1Type, param2Type);` Each function signature must be followed by a semicolon.

9. All other top level elements are functions. They take the form `<return type><whitespace mandatory><name><whitespace optional>(<argument list>)<whitespace optional>{<body>}`

	a. `<return type>` is the type the function returns. It must be defined in the source file or imported. All code paths must return this type. A type of `void` should be used to indicate nothing will be returned.

	b. `<name>` is the name of the function. It must be a valid and unique identifier.

	c. `<argument list>` is a list of parameters, each with the form `<param><whitespace optional>:<whitespace optional><type>` with a comma after each. There may not be a comma after the last parameter.

	d. `<body>` is the code the function executes.

#III. Statements and Control Flow

1. Values are either identifiers (defined I.4), character literals (I.5), string literals (I.6), or number literals (defined I.7).

2. Expressions are defined as either a function call, a value, or two values joined by an operator.

	a. The type of an expression is determined by the return type of its call, the type of its value, or the type returned by its operator.

3. Statements are an assignment, a bind, or a function call delineated by a semicolon, or a control construct or block.

4. A block is a piece of code set off by brackets (`{` and `}`) with zero or more statements between them.

5. An `if` statement is a control construct that executes a statement if a given boolean expression evaluates to true. It takes the form `if<whitespace optional>(<boolean expression>)<whitespace optional><statement>`.

6. A `while` statement is a control construct that executes a statement while a given boolean expression evaluates to true. It takes the form `while<whitespace optional>(<boolean expression>)<whitespace optional><statement>`.

#IV. Bindings and Assignments

1. Bindings are how variables are created. They either take the form `let<whitespace mandatory><variable name><whitespace optional>=<whitespace optional><expression><whitespace optional>;` or `let<whitespace mandatory><variable name><whitespace optional>:<whitespace optional><type name><whitespace optional>=<expression><whitespace optional>;`.

	a. If no type name is provided, the compiler will infer the type of the variable based on the value of the right-hand expression.

	b. The variable name must be unique in the current context.

	c. The variable name may incorporate only the dot (`.`) and namespace (`::`) operators.

2. Any variable that has been created by a binding can be assigned to. Assignments take the form of `<variable name><whitespace optional>=<whitespace optional><expression><whitespace optional>;`

	a. The variable referred to by `<variable name>` must have been created using `let` before the assignment.

	b. The expression referred by `<expresssion>` must have the same return type as the type of the variable on the left.

	c. The variable name may incorporate only the dot (`.`) and namespace (`::`) operators.

#V. Operators

1. Operators can be used on a single expression, or to combine two expressions.

	a. Operators on a single expression are called "unary operators."

	b. Operators on two expressions are called "binary operators."

2. The miscellaneous unary operators in the language are `*<expression>` (dereference), `&<variable>` (reference), and `<expression>[<expression>]` (index).

	a. The dereference operator returns the value a pointer points to. It cannot be implemented for non-pointer types.

	b. The reference operator returns the address in memory where a variable is stored. It cannot be implemented for non-value types.

	c. The index operator returns a value that maps to a specific key. It can be implemented by the `__index__(Type t, IndexType i)` where `Type` is the type you wish to implement the operator for and `IndexType` is the type of the expression passed as the index.

3. The arithmetic operators are `<expression>+<expression>` (plus), `-<expression>` (unary minus), `<expression>-<expression>` (minus), `<expression>*<expression>` (multiply), `<expression>/<expression>` (divide), and `<expression>**<expression>` (exponent).

	a. Each operator does exactly what you would expect. `**` is the exponent operator.

	b. To be able to use the operators for user-defined types, use: `__plus__(Type a, Type b)`, `__uminus__(Type a)`, `__minus__(Type a, Type b)`, `__mult__(Type a, Type b)`, `__div__(Type a, Type b)`, and `__pow__(Type a, Type b)`

4. The logical operators are `<expression>&&<expression>` (logical and), `<expression>||<expression>` (logical or), and `!<expression>` (not operator).

	a. The operators do exactly what you would expect.

	b. To be able to use the operators for user-defined types, use: `__l_and__(Type a, Type b)`, `__l_or__(Type a, Type b)`, and `__not__(Type t)`.

5. The bitwise operators are `<expression>>><expression` (right shift), `<expression>>>><expression>` (right unsigned shift), `<expression><<<expression>` (left shift), `<expression>&<expression>` (bitwise and), `~<expresson>` (bitwise negation operator), `<expression>|<expression>` (bitwise or), and `<expression>^<expression>` (bitwise xor).

6. The comparison operators are `<expression>==<expression>` (equals), `<expression>!=<expression>` (not equals), `<expression>><expression>` (greater), `<expression><<expression>` (less), `<expression>>=<expression>` (greater than or equals), and `<expression><=<expression>` (less than or equals).
