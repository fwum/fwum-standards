#Fwum's Official Language Standards

This document contains the standards for the Fwum language.

The main page is [on Github](https://github.com/fwum/fwum-standards), but the project is also [on Gitlab](https://gitlab.com/fwum/fwum-standards/) and [Bitbucket](https://bitbucket.org/fwum/fwum-standards) to provide an alternative if Github goes down or a free software hosting service for those who prefer not to use proprietary software.

##Relationship to the Fwum compiler

This project is partially independent of the Fwum compiler, but they are obviously related. This standard defines the Fwum language (similar to ANSI or POSIX standards for the C language). The Fwum compiler project, on the other hand, is our attempt to meet this standard.

##Reasons for this project to exist

This project exists for two reasons: to define behavior, and to allow for alternate compilers.

###Defining Behavior

If the only source for the Fwum language was the compiler we create and maintain, any bug in the compiler would be ambiguous. An end user would have to question whether the unintuitive behavior was a language quirk or compiler bug. With a standard to reference, they can report their issue or know that it is a feature, not a bug.

###Alternate Compilers

While Fwum's compile-to-C nature means that it will almost never require much effort to port the compiler, there are many other reasons to have to write a compiler to target the language. Any such project would greatly benefit from this document.

##Structure of the project

- [Syntax](SYNTAX.md):

	I. Code, non-code, and definitions

	II. Top-level elements

	III. Statements and control flow

	IV. Bindings and Assignments

	V. Operators

- Names and Identifiers

	VI. Valid identifiers

	VII. Rserved identifiers

	VIII. Namespaces

- Import and Other Files

	IX. Imports

	X. Compiler Search Path

	XI. C Bindings

- Types and memory

	XII. Types and structs

	XIII. Templates

- Errors

	XIV. Compiler Errors
